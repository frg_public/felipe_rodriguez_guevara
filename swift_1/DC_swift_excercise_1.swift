/*
Two arrays are called similar if one can be obtained from another by swapping at most one pair of elements in one of the arrays.
Given two arrays a and b, check whether they are similar.
e.g
● For a = [1, 2, 3] and b = [1, 2, 3], the output should be
areSimilar(a, b) = true.
The arrays are equal, no need to swap any elements.
● For a = [1, 2, 3] and b = [2, 1, 3], the output should be
areSimilar(a, b) = true.
We can obtain b from a by swapping 2 and 1 in b.
● For a = [1, 2, 2] and b = [2, 1, 1], the output should be
areSimilar(a, b) = false.
Any swap of any two elements either in a or in b won't
make a and b equal.
Input/Output
● [input] array.integer a
Array of integers.
Guaranteed constraints:
3 ≤ a.length ≤ 10
5
,
1 ≤ a[i] ≤ 1000.
● [input] array.integer b
Array of integers of the same length as a.
Guaranteed constraints:
b.length = a.length,
1 ≤ b[i] ≤ 1000.
● [output] boolean
o true if a and b are similar, false otherwise.
*/


import Foundation

let a: [Int] = [1,2,3]
let b: [Int] = [1,3,2]

let output: Bool = areSimilar(a: a, b: b)
print("Input")
print("A:", a.description)
print("B:", b.description)
print("Output: ", output)

func areSimilar(a: [Int], b: [Int])-> Bool {
    var aDiff: [Int] = []
    var bDiff: [Int] = []
    var maxDiff = 0
    for index in 1...a.count-1 where a[index] != b[index] {
        maxDiff += 1
        //If the are more than 2 diferent elements arrays are not similar.
        if(maxDiff > 2) {
            print("number of diferent elements:", maxDiff)
            return false
            }
        aDiff.append(a[index])
        bDiff.append(b[index])
    }
    //Both array are equals
    if maxDiff == 0 { return true }
    //No pair elements to swap
    if maxDiff < 2 { return false }

    print("Elements of A that are diferent:", aDiff.description)
    print("Elements of B that are diferent:", bDiff.description)
    //If the diferent elements sorted are equals the arrays are similar
    return aDiff.sorted() == bDiff.sorted()

}