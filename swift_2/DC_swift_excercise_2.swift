/*
Given a string, find the number of different characters in it.
e.g.
For s = "cabca", the output should be
differentSymbolsNaive(s) = 3.
There are 3 different characters a, b and c.
Input/Output
● [input] string s
o A string of lowercase English letters.
o Guaranteed constraints:
3 ≤ s.length ≤ 1000.
● [output] integer

*/

let s = "cabca"
let output = differentSymbolsNaive(s: s)

print("Input:",s)
print("Output", output)

func differentSymbolsNaive(s: String)-> Int {
    //Set() is a Collection of unique elements. So we make a set of the characters of the string. And then return the count.
    let outputString = Set(s)
    //Another solution whithout Set().
    /*
    val outputString = ""
    for chars in array {
        if(!outputString.contains(chars)) {
            outputString.append(chars)
        }
    }
    */
    print("Input with duplicates removed", outputString)
    return outputString.count
}