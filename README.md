# felipe_rodriguez_guevara

### Full Stack Mobile Test
You can run swift files in swift playgrounds in XCODE on an online tool.
and kt files in kotlin playground online ( https://play.kotlinlang.org/ ).

## Use Java o Kotlin
1. Given an array of strings, return another array containing all of its longest
strings.
e.g.
For inputArray = ["aba", "aa", "ad", "vcd", "aba"], the output should be
allLongestStrings(inputArray) = ["aba", "vcd", "aba"].
Input/Output
● [input] array.string inputArray
A non-empty array.
Guaranteed constraints:
1 ≤ inputArray.length ≤ 10,
1 ≤ inputArray[i].length ≤ 10.
● [output] array.string
o Array of the longest strings, stored in the same order as in
the inputArray.

2. Given an array of integers, replace all the occurrences
of elemToReplace with substitutionElem.
e.g.
For inputArray = [1, 2, 1], elemToReplace = 1, and substitutionElem = 3, the
output should be
arrayReplace(inputArray, elemToReplace, substitutionElem) = [3, 2, 3].
Input/Output
● [input] array.integer inputArray
Guaranteed constraints:
0 ≤ inputArray.length ≤ 10
4
,
0 ≤ inputArray[i] ≤ 10
9
.
● [input] integer elemToReplace
Guaranteed constraints:
0 ≤ elemToReplace ≤ 10
9
.
● [input] integer substitutionElem
Guaranteed constraints:
0 ≤ substitutionElem ≤ 10
9
.
## Using Swift
1. Two arrays are called similar if one can be obtained from another by swapping
at most one pair of elements in one of the arrays.
Given two arrays a and b, check whether they are similar.
e.g
● For a = [1, 2, 3] and b = [1, 2, 3], the output should be
areSimilar(a, b) = true.
The arrays are equal, no need to swap any elements.
● For a = [1, 2, 3] and b = [2, 1, 3], the output should be
areSimilar(a, b) = true.
We can obtain b from a by swapping 2 and 1 in b.
● For a = [1, 2, 2] and b = [2, 1, 1], the output should be
areSimilar(a, b) = false.
Any swap of any two elements either in a or in b won't
make a and b equal.
Input/Output
● [input] array.integer a
Array of integers.
Guaranteed constraints:
3 ≤ a.length ≤ 10
5
,
1 ≤ a[i] ≤ 1000.
● [input] array.integer b
Array of integers of the same length as a.
Guaranteed constraints:
b.length = a.length,
1 ≤ b[i] ≤ 1000.
● [output] boolean
o true if a and b are similar, false otherwise.
2. Given a string, find the number of different characters in it.
e.g.
For s = "cabca", the output should be
differentSymbolsNaive(s) = 3.
There are 3 different characters a, b and c.
Input/Output
● [input] string s
o A string of lowercase English letters.
o Guaranteed constraints:
3 ≤ s.length ≤ 1000.
● [output] integer


## Android
1. Convert the next view into a Mobile App using Android studio
(xml) and Xcode (storyboard). You can change colors and icons.

2. Create an app for Android (Java or Kotlin) and iOS (Swift) which can read QR codes
and display the info on screen. You are freely to make the screens design.

## Swift
My Apologies. I dont own the required hardware (Mac OS with XCODE) to develop native projects for IOS.