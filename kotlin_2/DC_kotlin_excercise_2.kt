/**
Given an array of integers, replace all the occurrences of elemToReplace with substitutionElem.
e.g.
For inputArray = [1, 2, 1], elemToReplace = 1, and substitutionElem = 3, the output should be
arrayReplace(inputArray, elemToReplace, substitutionElem) = [3, 2, 3].

Input/Output
● [input] array.integer inputArray
Guaranteed constraints:
0 ≤ inputArray.length ≤ 10
4
,
0 ≤ inputArray[i] ≤ 10
9
.
● [input] integer elemToReplace
Guaranteed constraints:
0 ≤ elemToReplace ≤ 10
9
.
● [input] integer substitutionElem
Guaranteed constraints:
0 ≤ substitutionElem ≤ 10
9
.
 */

fun main() {
    val inputArray: Array<Int> = arrayOf(1,2,1)
    val output = inputArray.arrayReplace(1,3)
    println("Input: ${inputArray.contentToString()}")
    println("Output:${output.contentToString()}")
}

private fun Array<Int>.arrayReplace(elemToReplace: Int, substitutionElem: Int ): Array<Int> {
    val output: ArrayList<Int> = arrayListOf()

    /* Approach using kotlin Collection functions */
    this.toCollection(output)
    output.replaceAll { item ->
        if(item == elemToReplace) substitutionElem else item
    }
    /**
    //Above approach will be the same as
    this.forEach { item ->
        output.add(
            if(item == elemToReplace) substitutionElem else item
        )
    }
    */
    return output.toTypedArray()
}