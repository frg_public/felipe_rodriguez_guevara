package com.frg.dcbarcode

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.activity.enableEdgeToEdge
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.ImageAnalysis.COORDINATE_SYSTEM_VIEW_REFERENCED
import androidx.camera.mlkit.vision.MlKitAnalyzer
import androidx.camera.view.LifecycleCameraController
import androidx.camera.view.PreviewView
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.frg.dcbarcode.databinding.ActivityMainBinding
import com.google.android.material.snackbar.Snackbar
import com.google.mlkit.vision.barcode.BarcodeScannerOptions
import com.google.mlkit.vision.barcode.BarcodeScanning
import com.google.mlkit.vision.barcode.common.Barcode

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private var lastUriSaved: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        checkForButton()
        if(ContextCompat.checkSelfPermission(baseContext, android.Manifest.permission.CAMERA)== PackageManager.PERMISSION_GRANTED) {
            getScanning()
        } else {
            launchPermissions()
        }

        binding.launchButton.setOnClickListener { launchBrowser() }
    }


    private fun launchPermissions() {
        requestPermissionLauncher.launch(android.Manifest.permission.CAMERA)
    }
    private fun getScanning(){

        val cameraController = LifecycleCameraController(baseContext)
        cameraController.bindToLifecycle(this)
        val preViewView: PreviewView = binding.uiPreview
        preViewView.controller = cameraController

        val option = BarcodeScannerOptions.Builder()
            .setBarcodeFormats(Barcode.FORMAT_QR_CODE)
            .build()
        val barCodeScanner = BarcodeScanning.getClient(option)

        cameraController.setImageAnalysisAnalyzer(
            ContextCompat.getMainExecutor(baseContext),
            MlKitAnalyzer(
                listOf(barCodeScanner),
                COORDINATE_SYSTEM_VIEW_REFERENCED,
                ContextCompat.getMainExecutor(baseContext)
            ) {
                val result = it.getValue(barCodeScanner)
                val success = !(result.isNullOrEmpty() || result.first() == null)
                if(success) {
                    result?.forEach {  barcode -> checkIfContainsUrl(barcode) }
                    checkForButton()
                }
            }
        )
    }

    private fun checkIfContainsUrl(barcode: Barcode) {
        if(barcode.valueType == Barcode.TYPE_URL) {
            lastUriSaved = barcode.rawValue.orEmpty()
            binding.lastUri.text = barcode.rawValue.orEmpty()
        }
    }
    private fun checkForButton() {
        binding.launchButton.visibility = if(lastUriSaved.isNotBlank()) View.VISIBLE else View.GONE
    }

    private fun launchBrowser() {
        if (lastUriSaved.isNotBlank()) {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(lastUriSaved))
            startActivity(browserIntent)
        }
    }
    private val requestPermissionLauncher = registerForActivityResult(ActivityResultContracts.RequestPermission()
        ) { isGranted: Boolean ->
            if (isGranted) {
                getScanning()
            } else {
                Snackbar.make(binding.root, "Se necesitan permisos", Snackbar.LENGTH_INDEFINITE).setAction("Aceptar") {
                    launchPermissions()
                }.show()
            }
        }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
}