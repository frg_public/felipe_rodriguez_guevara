/**
Given an array of strings, return another array containing all of its longest strings. e.g.
For inputArray = ["aba", "aa", "ad", "vcd", "aba"], the output should be allLongestStrings(inputArray) = ["aba", "vcd", "aba"].
Input/Output
● [input] array.string inputArray
A non-empty array.
Guaranteed constraints:
1 ≤ inputArray.length ≤ 10,
1 ≤ inputArray[i].length ≤ 10.
● [output] array.string
o Array of the longest strings, stored in the same order as in
the inputArray.
 * */

fun main() {
    val inputArray: Array<String> = arrayOf("aba", "aa", "ad", "vcd", "aba")
    val outputArray: Array<String> = inputArray.allLongestStrings()
    println("Input: ${inputArray.contentToString()}")
    println("Output: ${outputArray.contentToString()}")
}

private fun Array<String>.allLongestStrings(): Array<String>{
    val outputArray: ArrayList<String> = arrayListOf()
    var maxLength = 0
    this.forEach { item ->
        when{
            item.length > maxLength -> {
                outputArray.clear()
                outputArray.add(item)
                maxLength = item.length
            }
            item.length == maxLength -> outputArray.add(item)
            else -> Unit
        }
    }
    return outputArray.toTypedArray()
}